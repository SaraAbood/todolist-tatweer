package com.tatweer.todolist.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tatweer.todolist.repository.TodoRepository

/*
* The Factory takes in the Repository as its parameter
**/

class TodoViewModelFactory(private val TaskRepository: TodoRepository) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TodoViewModel(TaskRepository) as T
    }
}