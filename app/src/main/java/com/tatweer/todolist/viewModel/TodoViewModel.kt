package com.tatweer.todolist.viewModel

import androidx.lifecycle.ViewModel
import com.tatweer.todolist.model.Todo
import com.tatweer.todolist.repository.TodoRepository


/*
* The view model takes the repository as its parameters
*/

class TodoViewModel(private val TaskRepository: TodoRepository) : ViewModel() {

    fun addTodo(task: Todo) = TaskRepository.addTodo(task)

    fun getTodos(query:String) = TaskRepository.getTasks(query)

}