package com.tatweer.todolist.repository

import com.tatweer.todolist.model.ToDoDao
import com.tatweer.todolist.model.Todo

/*
* class takes the DAO as its parameters
* */

class TodoRepository(private val toDoDao: ToDoDao) {

    fun addTodo(todo: Todo) = toDoDao.addTodo(todo)

    fun getTasks(query:String) = toDoDao.getTodos(query)

    companion object {
        @Volatile
        private var instance: TodoRepository? = null

        fun getInstance(taskDao: ToDoDao) = instance ?: synchronized(this) {
            instance ?: TodoRepository(taskDao).also { instance = it }
        }
    }

}