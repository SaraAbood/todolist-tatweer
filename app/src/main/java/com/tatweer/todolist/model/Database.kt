package com.tatweer.todolist.model

import android.content.Context

/*
* This is basically a singleton that will enable us to initialize the Dao from it and only from it.
* */

class Database(context: Context) {

    var todoDao = ToDoDao(context)

    companion object {
        @Volatile
        private var instance: Database? = null

        fun getInstance(context: Context) = instance
            ?: synchronized(this) {
            instance
                ?: Database(context).also { instance = it }
        }
    }
}