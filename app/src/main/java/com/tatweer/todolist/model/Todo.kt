package com.tatweer.todolist.model

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate

/*
* this class that will represent a table on our database
*/

data class Todo(var title: String, var description: String, var deadline: LocalDate, var completed: Boolean ) {
    var id = 0

    @RequiresApi(Build.VERSION_CODES.O)
    constructor() : this("", "", LocalDate.now(), false)

    override fun toString(): String {
        return "${title} - ${deadline.toString()} \r\n ${description}"
    }
}