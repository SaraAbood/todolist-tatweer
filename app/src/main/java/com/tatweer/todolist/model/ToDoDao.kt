package com.tatweer.todolist.model

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

val DB_NAME = "ToDos"
val TABLE_NAME = "Todo"
var COL_ID = "id"
var COL_TITLE = "title"
var COL_DESCRIPTION = "description"
var COL_DEADLINE = "deadline"
const val COL_COMPLETED = "completed"

/*
* this is the database class,  It contains functions that enables us to manipulate the database
* */

class ToDoDao(context: Context) : SQLiteOpenHelper(context,
    DB_NAME, null, 2) {

    private var taskList: MutableList<Todo> = ArrayList()

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable =
            "CREATE TABLE ${TABLE_NAME}(${COL_ID} INTEGER PRIMARY KEY AUTOINCREMENT, ${COL_TITLE} VARCHAR(256),${COL_DESCRIPTION} VARCHAR(256), ${COL_DEADLINE} VARCHAR(256), ${COL_COMPLETED} BOOLEAN);"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }


    fun addTodo(todo: Todo) {
        val db = this.writableDatabase
        var contentValues = ContentValues()
        contentValues.put(COL_TITLE, todo.title)
        contentValues.put(COL_DESCRIPTION, todo.description)

        contentValues.put(COL_DEADLINE, todo.deadline.toString())
        contentValues.put(COL_COMPLETED, todo.completed)

        db.insert(TABLE_NAME, null, contentValues)

    }


        @SuppressLint("NewApi")
    fun getTodos(searchQuery:String): MutableList<Todo> {
        val db = this.readableDatabase
            taskList.clear()
        var query = ""
            query = if (searchQuery.isNullOrEmpty())
            {

                "SELECT * FROM ${TABLE_NAME};"
            }else
            {

                "SELECT * FROM $TABLE_NAME WHERE $COL_TITLE like '%${searchQuery}%' or $COL_DESCRIPTION like '%${searchQuery}%';"
            }
        val result = db.rawQuery(query, null)
            if (result != null && result.moveToFirst()){
                do{
                    var todo = Todo()
                    todo.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
                    todo.title = result.getString(result.getColumnIndex(COL_TITLE))
                    todo.description = result.getString(result.getColumnIndex(COL_DESCRIPTION))
                    todo.deadline = LocalDate.parse(result.getString(result.getColumnIndex(COL_DEADLINE)), DateTimeFormatter.ISO_DATE)
                    todo.title = result.getString(result.getColumnIndex(COL_TITLE))

                    taskList.add(todo)
                }while(result.moveToNext())

            }


        result.close()
        db.close()

        return taskList
    }

}