package com.tatweer.todolist.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.tatweer.todolist.R
import com.tatweer.todolist.model.Todo

class TodoAdapter(private val context: Context,
                  private val dataSource: MutableList<Todo>): BaseAdapter(){


    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.list_item, parent, false)

        val titleTextView = rowView.findViewById(R.id.title) as TextView

        val descriptionTextView = rowView.findViewById(R.id.description) as TextView

        val deadlingTextView = rowView.findViewById(R.id.date) as TextView

        val todo = getItem(position) as Todo

        titleTextView.text = todo.title
        descriptionTextView.text = todo.description
        deadlingTextView.text = todo.deadline.toString()


        return rowView    }



}