package com.tatweer.todolist.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.textChanges
import com.tatweer.todolist.R
import com.tatweer.todolist.model.Todo
import com.tatweer.todolist.utils.InjectionUtils
import com.tatweer.todolist.viewModel.TodoViewModel
import kotlinx.android.synthetic.main.activity_todolist.*

class TodoListActivity : AppCompatActivity() {

    private var searchQuery:String=""

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todolist)
        RxTextView.textChanges(searchText).subscribe()

        searchText.textChanges().subscribe(
            {
                searchQuery=it.toString()
                refreshData()
            },
            {
                Log.d("TextChanges",it.message)
            }
        )
    }
    private fun refreshData(){
        var data = getItems(searchQuery)
        displayToDos(data)
    }
    override fun onResume() {
        super.onResume()
        searchText.text = null
        refreshData()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_buttons, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_add-> {
            startActivity(Intent(this,AddToDoActivity::class.java))

            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    private fun getItems(searchQuery:String):MutableList<Todo> {

        val factory = InjectionUtils.provideViewModelFactory()
        val viewModel = factory.create(TodoViewModel::class.java)
        val data = viewModel.getTodos(searchQuery)

        return data
    }
    private fun displayToDos(list: MutableList<Todo>){
        val adapter = TodoAdapter(this, list)
        todo_list.adapter = adapter



        if(todo_list.adapter.count == 0){
            todo_list.visibility = View.INVISIBLE
           noData_textview.visibility = View.VISIBLE

       } else {
           todo_list.visibility = View.VISIBLE
           noData_textview.visibility = View.INVISIBLE

       }
    }
}
