package com.tatweer.todolist.view

import android.app.DatePickerDialog
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.tatweer.todolist.model.Todo
import com.tatweer.todolist.utils.InjectionUtils
import com.tatweer.todolist.viewModel.TodoViewModel
import kotlinx.android.synthetic.main.activity_add_to_do.*
import androidx.lifecycle.ViewModelProviders
import java.text.SimpleDateFormat
import java.util.*
import java.time.LocalDate






class AddToDoActivity : AppCompatActivity() {
    //  var instance:AddToDoActivity

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.tatweer.todolist.R.layout.activity_add_to_do)
        add_new_todo.setOnClickListener { addTodos() }
       // deadline_edittext.text = SimpleDateFormat("dd.MM.yyyy").format(System.currentTimeMillis())

        var cal = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            deadline_edittext.text = sdf.format(cal.time)

        }
        deadline_edittext.setOnClickListener{ DatePickerDialog(this, dateSetListener,
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)).show()}
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addTodos() {

        val factory = InjectionUtils.provideViewModelFactory()
        val viewModel = ViewModelProviders.of(this, factory).get(TodoViewModel::class.java)

        if(title_edittext.text.toString().isNullOrEmpty()
            || title_edittext.text.toString().isNullOrEmpty()
            || deadline_edittext.text.toString().isNullOrEmpty())
        {

            Toast.makeText(this,"Please Fill all Fields", Toast.LENGTH_SHORT).show()
            return

        }

        val taskObject = Todo(title_edittext.text.toString(), description_edittext.text.toString(),
            LocalDate.parse(deadline_edittext.text.toString()) ,false)
        viewModel.addTodo(taskObject)

        Toast.makeText(this,"ToDo Added Successfully", Toast.LENGTH_SHORT).show()
        this.finish()

    }



}
