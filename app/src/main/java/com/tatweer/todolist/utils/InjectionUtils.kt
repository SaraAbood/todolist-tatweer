package com.tatweer.todolist.utils

import com.tatweer.todolist.model.Database
import com.tatweer.todolist.repository.TodoRepository
import com.tatweer.todolist.viewModel.TodoViewModelFactory


/*
* this class will enable us to use the factory
* */
object InjectionUtils {
    fun provideViewModelFactory() : TodoViewModelFactory {
        val repository = TodoRepository.getInstance(Database.getInstance(MainApplication.applicationContext()).todoDao)
        return TodoViewModelFactory(repository)
    }
}